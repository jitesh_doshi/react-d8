//App component
class App extends React.Component {
  constructor(props) {
    super(props); // call super-constructor
    // initialize state
    this.state = {
      nodes: []
    }
  }

  // callback for initializing this component
  componentWillMount(){
    let self = this; //Once inside the axios request, this will not be the same this.
    axios.get('/node/rest')
    .then(function (response) {
      // set new state
      self.setState({
        nodes: response.data
      })
    });
  }

  render () {
    // JSX
    return (
      <div className='row'>
        <div className='col-md-6'>
          <h2>Node Creation Form</h2>
          <NewNodeForm />
          <p><a href='/'>Site Front Page</a></p>
          <p><a href='/admin/content'>Manage Nodes</a></p>
          <p><a href='/admin/config/services/rest'>REST Configuration</a></p>
        </div>
        <div className='col-md-6'>
          <h2>Recent Nodes</h2>
          <NodeList nodes={this.state.nodes} />
        </div>
      </div>
    ) 
  }
}

// NodeList functional component
const NodeList = ({ nodes }) => {
  return (
    <ul>{nodes.map(n => <li key={n.nid}><Node node={n}/></li>)}</ul>
  )
}
// Node Component
const Node = ({ node }) => {
  return (
    <div>
      <h4><a href={'/node/' + node.nid}>{node.title}</a></h4>
      {/* <div>{node.body}</div> */}
    </div>
  )
}
// Form Component
class NewNodeForm extends React.Component{
  constructor(props){
    super(props); // call super-constructor
    // initialize state
    this.state = {
      title: '',
      body: '',
      csrfToken: ''
    }
    // ReactJS is forgetful about who 'this' is. So the following is needed.
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  
  // callback to initialize the component
  componentWillMount() {
    let self= this;
    axios.get('/rest/session/token')
    .then(function (response) {
      const csrf_token = response.data;
      // console.log({csrf_token});
      self.setState({'csrfToken': csrf_token});
    });
  }
  render() {
    return(
      <form onSubmit={this.handleSubmit} className='well'>
        <div className='form-group'>
          <label htmlFor="title">Title</label>
          <input name="title" className='form-control' onChange={this.handleChange} placeholder='enter title of the new node'/>
        </div>
        <div className='form-group'>
          <label htmlFor="body">Body</label>
          <textarea name="body" className='form-control' onChange={this.handleChange} placeholder='enter basic_html body of the new node'/>
        </div>
        <div  className='form-group'>
          <button type='submit' className='btn btn-primary'>Submit</button>
        </div>
      </form>
    )
  }

  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value});
  }

  handleSubmit(e) {
    e.preventDefault();
    let self = this.state;
    // construct the node object
    let node = {
      "type":[{"target_id":"article","target_type":"node_type"}],
      "title":[{"value": self.title}],
      "body":[{"value": self.body}]
    }
    let config = {
      headers: {'X-CSRF-Token': self.csrfToken}
    }
    // post the node to server as JSON
    axios.post('/entity/node?_format=json', node, config)
    .then(function(success) {
      console.log(success);
      window.location.reload();
    });
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
